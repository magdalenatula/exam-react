import React, { useEffect, useState, useContext, Component } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import Navigation from "../../components/Navigation";
import RandomRecipe from "./components/RandomRecipe";

const WrapperRandomRecipes = styled("div")`
position:relative;
background: white;
display: flex;
h1 {
    color: #313842;
}
  }
background: linear-gradient(
    to right bottom,
    rgba(255,255,255, 0.5),
    rgba(255,255,255, 0.3)
    );
backdrop-filter: blur(.2rem);
width: 85vw;
height: 85vh;
margin: auto;
border-radius: 20px;
z-index:21;
margin-top:4em;
`
const RecipesItemsWrapper = styled("div")`
    justify-content: space-around;
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-row-gap: 3%;
    grid-column-gap: 3%;
    margin:auto;
    margin-top:3em;
    position: relative;
    height: 67vh;
    overflow: auto;
    border-radius: 20px;
    padding: 1em;

`;

const RandomRecipes = () => {
    const [recipes, setRecipes] = useState([]);
    let history = useHistory();

    useEffect(() => {
        const showRecipes = async () => {
            try {
                const response = await fetch(`http://www.recipepuppy.com/api/`);
                const data = await response.json();
                setRecipes(data.results)
            } catch (e) {
                setRecipes(false)
                console.log(e);
            }
        }
        showRecipes();
    }, []);

    return (
        <WrapperRandomRecipes>
            <Navigation history={history} />

            <div>
                <h1>Random Recipes</h1>
                <RecipesItemsWrapper>
                    {recipes && recipes.map((recipe, index) => (
                        <RandomRecipe key={index} recipe={recipe} />
                    ))}
                </RecipesItemsWrapper>
            </div>
        </WrapperRandomRecipes>
    );
}

export default RandomRecipes;