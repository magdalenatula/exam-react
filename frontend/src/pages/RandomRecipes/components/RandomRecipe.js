import React from "react";
import styled from "styled-components";

const OneItemBox = styled("div")`
    background-color: white;
    padding: .5em;
    border-radius: 20px;
    align-content: center;
`;

const CircleButton = styled("a")`
    display: block;
    margin: auto;
    width:120px;
    padding: .6em 1em;
    background: #6B2CA6;
    color: white;
    border-radius: 20px;
    border: none;
    font-weight: bold;
    &:hover {
        opacity: 0.7;
    }
    &:focus {
        outline: none;
    }
    text-decoration: none;
`

function RandomRecipe(props) {
    const { recipe } = props;
    return (

        <OneItemBox>
            <h4> {recipe.title}</h4>
            <hr></hr>
            <p>{recipe.ingredients}</p>
            <CircleButton href={recipe.href} target="_blank" >Open Recipe</CircleButton>
        </OneItemBox>
    )
}

export default RandomRecipe;