import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { UserContext } from "../../common/UserContext";
import Navigation from "../../components/Navigation";
import HouseholdMembers from "./components/HouseholdMembers"

const WrapperProfile = styled("div")`
position:relative;
background: white;
display: flex;
h1 {
    color: #313842;
}
  }
background: linear-gradient(
    to right bottom,
    rgba(255,255,255, 0.5),
    rgba(255,255,255, 0.3)
    );
backdrop-filter: blur(.2rem);
width: 85vw;
height: 85vh;
margin: auto;
border-radius: 20px;
z-index:21;
margin-top:4em;
`

const WrapperProfileDiv = styled("div")`
width:100%;
position: relative;
overflow: auto;
border-radius: 20px;
padding: .5em;
margin: auto;
margin-top: 1em;
`
const BoldText = styled("span")`
font-weight: bold;
`

const Users = () => {
    const { user } = useContext(UserContext);
    const [profile, setProfile] = useState([]);
    const history = useHistory();

    const showUserProfile = async () => {
        try {
            const response = await fetch(`auth/profile/${user}`);
            const data = await response.json();
            setProfile(data)
        } catch (e) {
            setProfile(null);
            console.log(e);
        }
    }

    useEffect(() => {
        showUserProfile();
    }, [user]);

    return (
        <WrapperProfile>
            <Navigation history={history} />
            <WrapperProfileDiv>
                <h1>Profile page</h1>
                {profile && profile.map((singleProfile, index) => (
                    <div key={index}>
                        <p><BoldText>Name </BoldText>{singleProfile.firstName} {singleProfile.lastName}</p>
                        <p><BoldText>E-mail </BoldText>{singleProfile.email}</p>
                        <p><BoldText>Household ID </BoldText>{singleProfile.listID}</p>
                    </div>
                ))}
                <HouseholdMembers />
            </WrapperProfileDiv>
        </WrapperProfile>
    );
}

export default Users;