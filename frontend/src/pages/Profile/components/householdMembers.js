import React, { useEffect, useState, useContext } from "react";
import { ListContext } from "../../../common/ListContext";

import styled from "styled-components";

const OneMemberBox = styled("div")`
background-color: rgba(255,255,255, 0.4); 
border-radius: 20px;
margin: auto;
width: 60%;
padding: 3px;
margin-bottom: 3px;

span {
    font-weight: bold;
}
`;

const HouseholdMembers = () => {
    const { list } = useContext(ListContext);
    const [members, setMembers] = useState([]);

    const showMembers = async () => {
        try {
            const response = await fetch(`auth/${list}`);
            const data = await response.json();
            setMembers(data);
        } catch (e) {
            setMembers(false);
            console.log(e);
        }
    }

    useEffect(() => {
        showMembers();
    }, [list]);

    return (
        <div>
            <h1>Members in your Household</h1>
            {members && members.map((member, index) => (
                <OneMemberBox key={index}>
                    <p><span>Name:</span> {member.firstName} {member.lastName} <span>E-mail:</span> {member.email}</p>
                </OneMemberBox>
            ))}
        </div>
    );
}

export default HouseholdMembers;