import React from "react";
import styled from "styled-components";
import { ReactComponent as EmptyFridgeIllustration } from "../../../assets/empty_fridge.svg";
import { useHistory } from "react-router-dom";

const WrapperDiv = styled("div")`
    .empty-fridge-illustration {    
        margin-top: 2em;
        height: 50%;
    }
    height: 61vh;
    margin:auto;

`


const EmptyFridge = () => {
    const history = useHistory();

    return (
        <WrapperDiv>
            <EmptyFridgeIllustration className="empty-fridge-illustration" />
            <h1>It’s Empty!</h1>
            <p>Looks like you haven’t saved
                any items yet… </p>

        </WrapperDiv>
    );
}

export default EmptyFridge;