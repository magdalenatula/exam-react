import React, { useState, useEffect } from "react";
import axios from 'axios';
import styled from "styled-components";

const QuantityInput = styled("input")`
    width: 50%;
    box-sizing: border-box;
    height: 40px;
   
    font-size: 0.9em;
    border: none;
    border-radius: 20px;
    padding: 2px 15px;
    background-color: rgba(255,255,255, 0.8); 
    &:focus {
        outline: none;
        box-shadow: 0 0 0 2px #8E47D1;
    }
    margin: auto;
    margin-bottom: .5em;
`

const CircleButton = styled("button")`
    margin: auto;
    margin-bottom: .5em;
    margin-left: .5em;
    height: 40px;
    padding: 0.5em 1em;
    background: #6B2CA6;
    color: white;
    border-radius: 20px;
    border: none;
    font-weight: bold;
    &:hover {
        opacity: 0.7;
    }
    &:focus {
        outline: none;
    }
`

const EditFridgeItem = ({ itemID, showFridgeItems, setEditItem }) => {

    const [updateItem, setUpdateItem] = useState(
        { quantity: 0, itemID: itemID }
    );

    const handleChange = (event) => {
        setUpdateItem({ ...updateItem, [event.target.name]: event.target.value })
    }


    const handleUpdateItem = async (event) => {
        event.preventDefault();
        console.log(updateItem)
        const connection = await axios.patch(`fridge/${itemID}`, updateItem);
        if (connection) {
            showFridgeItems();
            setTimeout(() => setEditItem(false), 1000);
        }

    };


    return (
        <div>
            <form onSubmit={handleUpdateItem}>
                <QuantityInput name="quantity" value={updateItem.quantity} type="number" pattern="[0-9]*" onChange={handleChange} required />
                <CircleButton type="submit">Update</CircleButton>
            </form>
        </div>
    )
};

export default EditFridgeItem;
