import React from "react";
import styled from "styled-components";
import Fridgeitem from './FridgeItem';

const FridgeItemsWrapper = styled("div")`
justify-content: space-around;
display: grid;
grid-template-columns: 1fr 1fr 1fr 1fr;
grid-row-gap: 3%;
grid-column-gap: 3%;
`;

const FridgeItems = (props) => {
    const { searchedItems, showFridgeItems } = props;
    return (
        <FridgeItemsWrapper>
            {searchedItems && searchedItems.map((fridgeitem, index) => (
                <Fridgeitem key={index} showFridgeItems={showFridgeItems} fridgeitem={fridgeitem} />
            ))}
        </FridgeItemsWrapper>
    );
}
export default FridgeItems;
