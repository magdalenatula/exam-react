import React, { useState } from "react";
import axios from 'axios';
import styled from "styled-components";
import EditFridgeItem from "./EditFridgeItem"
import { ReactComponent as DeleteIcon } from "../../../assets/ico_delete.svg";
import { ReactComponent as EditIcon } from "../../../assets/ico_edit.svg";

const OneItemBox = styled("div")`
    background-color: white;
    border-style: solid;
    padding: .5em;
    border-radius: 20px;
    align-content: center;
    display: grid; 
    grid-template-columns: 60% 40%;

`;
const Buttons = styled("div")`
    border-left-style: solid;
    border-width: thin;  
    border-color: lightgrey;
    display:grid;
    grid-template-rows: 1fr 1fr;
    padding:1em;  
    grid-row-gap: 1em
    margin: auto; 
`;


const Fridgeitem = (props) => {
    const { fridgeitem, showFridgeItems } = props;
    const [editItem, setEditItem] = useState(false);

    const handleDeleteItem = async () => {
        const connection = await axios.delete(`fridge/${fridgeitem.itemID}`)
        // return connection;
        showFridgeItems();
    };
    const handleEditItem = async () => {
        setEditItem(true);
    }

    return (
        <div>
            {editItem && <EditFridgeItem setEditItem={setEditItem} showFridgeItems={showFridgeItems} itemID={fridgeitem.itemID} />}
            <OneItemBox className={`
                    ${fridgeitem.foodCategoryName === "Vegetables" ? "Vegetables" : ""} 
                    ${fridgeitem.foodCategoryName === "Fruit" ? "Fruit" : ""}
                    ${fridgeitem.foodCategoryName === "Meat" ? "Meat" : ""}
                    ${fridgeitem.foodCategoryName === "Dairy" ? "Dairy" : ""}
                    ${fridgeitem.foodCategoryName === "Fish" ? "Fish" : ""}
                    `}>
                <div>
                    <p>{fridgeitem.foodName}</p>
                    <p>{fridgeitem.quantity}</p>
                </div>
                <Buttons>
                    <EditIcon onClick={handleEditItem} className="ico_edit" />
                    <DeleteIcon onClick={handleDeleteItem} className="ico_delete" />
                </Buttons>
            </OneItemBox>
        </div>
    );
}

export default Fridgeitem;
