import React, { useEffect, useState, useContext } from "react";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { ListContext } from "../../common/ListContext";
import FridgeItems from './components/FridgeItems';
import EmptyFridge from "./components/EmptyFridge"
import Navigation from "../../components/Navigation";

const WrapperFridge = styled("div")`
position:relative;
background: white;
display: flex;
h1 {
    color: #313842;
}
background: linear-gradient(
    to right bottom,
    rgba(255,255,255, 0.5),
    rgba(255,255,255, 0.3)
    );
backdrop-filter: blur(.2rem);
width: 85vw;
height: 85vh;
margin: auto;
border-radius: 20px;
z-index:21;
margin-top:4em;
`

const WrapperItemsDiv = styled("div")`
position: relative;
height: 59vh;
overflow: auto;
border-radius: 20px;
padding: 1em;
margin: auto;
margin-top: 1em;
`

const SearchDiv = styled("div")`
display:flex;
padding:1em;
`

const SearchInput = styled("input")`
box-sizing: border-box;
width: 30%;
height: 40px;
margin-right:1em;
font-size: 0.9em;
border: none;
border-radius: 20px;
padding: 2px 15px;
background-color: rgba(255,255,255, 0.4); 
&:focus {
    outline: none;
    box-shadow: 0 0 0 2px #8E47D1;
  }
`
const CircleButton = styled("button")`
display: block;
margin: auto;
height: 40px;
padding: 0.5em 2em;
background: #5857D3;
color: white;
border-radius: 20px;
border: none;
font-weight: bold;
&:hover {
    opacity: 0.7;
}
&:focus {
    outline: none;
}
`

const FilterButtons = styled("div")`
button{
    padding: .3em 1.5em;
    border-radius: 8px;
    border-style: solid;
    background-color: white;
    margin-right: .5em;
    margin-top: .5em;
}
`

const Fridge = () => {
    const { list } = useContext(ListContext);
    const [fridge, setFridge] = useState([]);
    const [search, setSearch] = useState("");
    const [filter, setFilter] = useState("");
    const [searchedItems, setSearchedItems] = useState([]);
    const history = useHistory();

    const showFridgeItems = async () => {
        try {
            const response = await fetch(`fridge/${list}`);
            const data = await response.json();
            setFridge(data);
        } catch (e) {
            setFridge(false);
            console.log(e);
        }
    }

    useEffect(() => {
        showFridgeItems();
    }, [list]);

    useEffect(() => {
        setSearchedItems(
            fridge.filter((fridgeitem) =>
                fridgeitem.foodName.toLowerCase().includes(search.toLowerCase())
            )
        );
    }, [search, fridge]);

    useEffect(() => {
        setSearchedItems(
            fridge.filter((fridgeitem => fridgeitem.foodCategoryName === filter))
        );
    }, [filter]);

    const handleResetFilter = async () => {
        showFridgeItems();
    };

    console.log(fridge)
    return (
        <WrapperFridge>
            <Navigation history={history} />
            <div>
                <div>
                    <h1>My Fridge</h1>
                    <SearchDiv>
                        <SearchInput
                            type="text"
                            placeholder="Search Items"
                            onChange={(e) => setSearch(e.target.value)}
                        />
                        <FilterButtons>
                            <button onClick={() => setFilter("Meat")} className="Meat">Meat</button>
                            <button onClick={() => setFilter("Vegetables")} className="Vegetables">Vegetables</button>
                            <button onClick={() => setFilter("Fish")} className="Fish">Fish</button>
                            <button onClick={() => setFilter("Fruit")} className="Fruit">Fruit</button>
                            <button onClick={() => setFilter("Dairy")} className="Dairy">Dairy</button>
                            <button onClick={handleResetFilter}>Reset Filters</button>
                        </FilterButtons>
                        <CircleButton onClick={() => history.push('/addfridgeitem')}>Add New Item</CircleButton>
                    </SearchDiv>
                </div>
                {!fridge.length ? <EmptyFridge /> :
                    <WrapperItemsDiv>
                        {<FridgeItems showFridgeItems={showFridgeItems} searchedItems={searchedItems} />}
                    </WrapperItemsDiv>}
            </div>
        </WrapperFridge>
    );
}

export default Fridge;