import React, { useState, useEffect, useContext } from "react";
import axios from 'axios';
import { Redirect } from "react-router-dom";
import { UserContext } from "../../common/UserContext";
import { ListContext } from "../../common/ListContext";
import styled from "styled-components";

const Headline = styled("h1")`
color: white;
margin: auto;
margin-top:20%;
`

const Logout = () => {
    const [redirect, setRedirect] = useState(false);
    const { setUser } = useContext(UserContext);
    const { setList } = useContext(ListContext);

    const logOut = async () => {
        const result = await axios.get("auth/logout");
        setUser(false);
        setList(false);
        console.log(result)
    }

    useEffect(() => {
        logOut();
        setTimeout(() => setRedirect(true), 2500);
    });

    return redirect ? (
        <Redirect to="/login" />) : (<Headline>see u!</Headline>
        )
};

export default Logout;