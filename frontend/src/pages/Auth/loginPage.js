import React, { useContext } from "react";
import axios from 'axios';
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { UserContext } from "../../common/UserContext";


const LoginWrapper = styled("div")`
text-align: center;
background-color: rgba(255,255,255, 0.6); 
border-radius: 30px;
margin: auto;
padding: 5em;
width: 40%;
margin-top: 3em;
`

const FormContainer = styled("form")`

  margin: 1em auto;
  padding: 2em;
  
  p{
    color: #E04F5F;
  }

  input {
    box-sizing: border-box;
    display: block;
    height: 40px;
    width: 100%;
    margin: 0.4em 0 1.3em;
    font-size: 0.9em;
    border: none;
    border-radius: 5px;
    padding: 2px 5px;
    background-color: white;
    &:focus {
      outline: none;
      box-shadow: 0 0 0 2px #8E47D1;
    }
  }
  label {
      display: flex;
      justify-self: left;
  }
 button{
  display: block;
  margin: auto;
  height: 40px;
  padding: 0.5em 2em;
  background: #5857D3;
  color: white;
  border-radius: 20px;
  border: none;
  font-weight: bold;
  &:hover {
      opacity: 0.7;
  }
  &:focus {
      outline: none;
  }
}
  }
`;

const SecondaryButton = styled("button")`
text-decoration: underline;
border: none;
color: #6B2CA6;
background: transparent;
`;

const Login = () => {
  const { setUser } = useContext(UserContext);
  const { register, handleSubmit, errors } = useForm();
  const history = useHistory();

  const onSubmit = async (data) => {
    console.log(data);
    const connection = await axios.post("/auth/login", data);
    console.log(connection)
    if (connection.data.userID) {
      setUser(connection.data.userID);
      history.push("/fridge");
    } else {
      console.log("error");
    }
  };

  return (
    <LoginWrapper>
      <h1>Login</h1>
      <FormContainer onSubmit={handleSubmit(onSubmit)}>
        <label htmlFor="email">Email</label>
        <input name="email" type="email" ref={register({ required: true })} />
        {errors.email && <p>Email field is required</p>}
        <label htmlFor="password">Password</label>
        <input name="password" type="password" ref={register({ required: true })} />
        {errors.password && <p>Password field name is required</p>}
        <button type="submit">Log In</button>
      </FormContainer>
      <p>Don't have an account?</p>
      <SecondaryButton onClick={() => history.push('/signup')}>Signup</SecondaryButton>
      <p>Do you want to signup to existing household?</p>
      <SecondaryButton onClick={() => history.push('/household')}>Signup to Household</SecondaryButton>
    </LoginWrapper>

  );
}

export default Login;