import React, { useState } from "react";
import axios from 'axios';
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import Confirmation from "../../components/Confirmation";
import ErrorMessage from "../../components/ErrorMessage";

const SignupWrapper = styled("div")`
text-align: center;
background-color: rgba(255,255,255, 0.6); 
border-radius: 30px;
margin: auto;
padding: 1em;
width: 40%;
margin-top: 1em;
`

const FormContainer = styled("form")`
  margin: 1em auto;
  padding: 2em;
  p{
    color: #E04F5F;
  }
  input {
    box-sizing: border-box;
    display: block;
    height: 40px;
    width: 100%;
    margin: 0.4em 0 1.3em;
    font-size: 0.9em;
    border: none;
    border-radius: 5px;
    padding: 2px 5px;
    background-color: white;
    &:focus {
      outline: none;
      box-shadow: 0 0 0 2px #8E47D1;
    }
  }
  label {
      display: flex;
      justify-self: left;
  }

  button{
    display: block;
    margin: auto;
    height: 40px;
    padding: 0.5em 2em;
    background: #5857D3;
    color: white;
    border-radius: 20px;
    border: none;
    font-weight: bold;
    &:hover {
        opacity: 0.7;
    }
    &:focus {
        outline: none;
    }
  }
  
`;
const SecondaryButton = styled("button")`
  text-decoration: underline;
  border: none;
  color: #6B2CA6;
  background: transparent;
`;

const Signup = () => {
  const [confirmation, setConfirmation] = useState(false);
  const [error, setError] = useState(false);
  const { register, handleSubmit, errors } = useForm();
  const history = useHistory();


  const onSubmit = async (data) => {
    const connection = await axios.post("/auth/signup", data);
    console.log(connection);
    if (connection.data === "exists") {
      console.log("Email already exists in our database")
      setError(true);
    } else {
      setConfirmation(true);
      setTimeout(() => setConfirmation(false), 2300);
      history.push("/login");
    }
  }

  return (
    <div>
      {confirmation && <Confirmation text={"You have created an account, now you can log in!"} />}
      {error && <ErrorMessage text={"Email already exists in our database!"} />}
      <SignupWrapper>
        <h1>Sign up</h1>
        <FormContainer onSubmit={handleSubmit(onSubmit)}>
          <label htmlFor="firstName">First Name</label>
          <input name="firstName" type="text" ref={register({ required: true })} />
          {errors.firstName && <p>First Name is required</p>}
          <label htmlFor="lastName">Last Name</label>
          <input name="lastName" type="text" ref={register({ required: true })} />
          {errors.lastName && <p>Last Name is required</p>}
          <label htmlFor="email">Email</label>
          <input name="email" type="email" ref={register({ required: true })} />
          {errors.email && <p>Email is required</p>}
          <label htmlFor="password">Password</label>
          <input name="password" type="password" ref={register({ required: true })} />
          {errors.password && <p>Password is required</p>}
          <label htmlFor="confirmPassword">Confirm Password</label>
          <input name="confirmPassword" type="password" ref={register({ required: true })} />
          {errors.confirmPassword && <p>Confirm Password is required</p>}
          <button type="submit">Sign Up</button>
        </FormContainer>
        <p>Don't have an account?</p>
        <SecondaryButton onClick={() => history.push('/login')}>Log in</SecondaryButton>
        <p>Do you want to sign up to existing household?</p>
        <SecondaryButton onClick={() => history.push('/household')}>Sign up to Household</SecondaryButton>
      </SignupWrapper>
    </div>

  );
}

export default Signup;