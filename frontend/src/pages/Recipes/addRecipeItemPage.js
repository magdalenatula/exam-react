import React, { useEffect, useState, useContext } from "react";
import axios from 'axios';
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { ListContext } from "../../common/ListContext";
import Navigation from "../../components/Navigation";
import Confirmation from "../../components/Confirmation";
import Ingridients from "./components/Ingridients"

const WrapperAddItem = styled("div")`
position:relative;
background: white;
display: flex;
h1 {
    color: #313842;
}
  }
background: linear-gradient(
    to right bottom,
    rgba(255,255,255, 0.5),
    rgba(255,255,255, 0.3)
    );
backdrop-filter: blur(.2rem);
width: 85vw;
height: 85vh;
margin: auto;
border-radius: 20px;
z-index:21;
margin-top:4em;
.empty-fridge-illustration {
    width: 20%;
  }
`
const FormWrapper = styled("form")`
    display: grid; 
    grid-template-columns: 1fr 1fr;
    grid-column-gap: 2em;
    width: 80%;
    margin:auto;
    margin-top:3em;
`
const LeftPart = styled("div")`
    background-color: rgba(255,255,255, 0.4); 
    border-radius: 20px;
    padding: 1em;
`
const CircleButton = styled("button")`
display: block;
margin: auto;
height: 40px;
padding: 0.5em 2em;
background: #5857D3;
color: white;
border-radius: 20px;
border: none;
font-weight: bold;
&:hover {
    opacity: 0.7;
}
&:focus {
    outline: none;
}
`
const Input = styled("input")`
    width: 100%;
    display: block;
    box-sizing: border-box;
    height: 40px;
    margin-right:1em;
    font-size: 0.9em;
    border: none;
    border-radius: 20px;
    padding: 2px 15px;
    background-color: rgba(255,255,255, 0.8); 
    &:focus {
        outline: none;
        box-shadow: 0 0 0 2px #8E47D1;
    }
    margin: auto;
    margin-bottom: 1em;
`
const RightSide = styled("div")`
width:100%;
`
const AddRecipeItem = () => {
    const { list } = useContext(ListContext);
    const { register, handleSubmit, errors } = useForm();
    const [confirmation, setConfirmation] = useState(false);

    const history = useHistory();

    const onSubmit = async (data) => {
        console.log(data);
        const connection = await axios.post(`recipes/addItem/`, data);
        console.log(connection);
        setConfirmation(true);
        setTimeout(() => setConfirmation(false), 2300);
    };

    return (
        <WrapperAddItem>
            <Navigation history={history} />
            <RightSide>
                <h1>Create New Recipe</h1>
                {confirmation && <Confirmation text={"New recipe has been saved!"} />}
                <FormWrapper onSubmit={handleSubmit(onSubmit)}>
                    <LeftPart>
                        <Input name="recipeName" type="text" placeholder="Title" ref={register({ required: true })} />
                        <Input name="recipeDescription" type="text-area" placeholder="Description" ref={register({ required: true })} />
                        <input type="hidden" value={list} name="recipeListID" ref={register()} />
                        <CircleButton type="submit">Add New Recipe</CircleButton>
                    </LeftPart>
                    <Ingridients register={register} />
                </FormWrapper>
            </RightSide>
        </WrapperAddItem>
    );
}

export default AddRecipeItem;
