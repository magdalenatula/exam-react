import React from "react";
import styled from "styled-components";
import RecipesItem from './RecipesItem';

const RecipesItemsWrapper = styled("div")`
justify-content: space-around;
display: grid;
grid-template-columns: 1fr 1fr;
grid-row-gap: 3%;
grid-column-gap: 3%;
`;

const RecipesItems = (props) => {
    const { recipes, showRecipes } = props;
    return (
        <RecipesItemsWrapper>
            {recipes && recipes.map((recipe, index) => (
                <RecipesItem key={index} recipe={recipe} showRecipes={showRecipes} />
            ))}
        </RecipesItemsWrapper>
    );
}

export default RecipesItems;