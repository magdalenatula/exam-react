import React from "react";
import styled from "styled-components";
import axios from 'axios';
import { ReactComponent as DeleteIcon } from "../../../assets/ico_delete.svg";

const OneItemBox = styled("div")`
    background-color: white;
    padding: .5em;
    border-radius: 20px;
    align-content: center;
    .title{
        position:relative;
    }
    .ico_delete {
        width:2em;
        position:absolute;
        top: -3px;
        right:2px;
    }

`;

const RecipesItem = (props) => {
    const { recipe, showRecipes } = props;

    const handleDeleteRecipe = async () => {
        console.log(recipe._id)
        const connection = await axios.delete(`recipes/${recipe._id}`);
        showRecipes();
    }
    console.log(recipe)

    return (
        <OneItemBox>
            <div className="title">
                <h4>{recipe.recipeName}</h4>
                <DeleteIcon onClick={handleDeleteRecipe} className="ico_delete" />
            </div>
            <hr></hr>
            {recipe.recipeIngridients && recipe.recipeIngridients.map((singleIngridient, index) => (
                <p key={index}>{singleIngridient}</p>
            ))}
            <hr></hr>
            <p>{recipe.recipeDescription}</p>
        </OneItemBox>
    );
}

export default RecipesItem;
