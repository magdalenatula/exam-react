import React, { useEffect, useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import { ListContext } from "../../common/ListContext";
import Navigation from "../../components/Navigation";
import RecipesItems from './components/RecipesItems';

const WrapperRecipes = styled("div")`
position:relative;
background: white;
display: flex;
h1 {
    color: #313842;
}
  }
background: linear-gradient(
    to right bottom,
    rgba(255,255,255, 0.5),
    rgba(255,255,255, 0.3)
    );
backdrop-filter: blur(.2rem);
width: 85vw;
height: 85vh;
margin: auto;
border-radius: 20px;
z-index:21;
margin-top:4em;
`
const WrapperItemsDiv = styled("div")`
position: relative;
height: 61vh;
overflow: auto;
border-radius: 20px;
padding: 1em;

margin: auto;
margin-top: 1em;
`

const CircleButton = styled("button")`
height: 40px;
padding: 0.5em 1em;
background: #6B2CA6;
color: white;
border-radius: 20px;
border: none;
font-weight: bold;
&:hover {
    opacity: 0.7;
  }
  &:focus {
    outline: none;
  }
`
const Recipes = () => {
    const { list } = useContext(ListContext);
    const [recipes, setRecipes] = useState([]);
    const history = useHistory();

    const showRecipes = async () => {
        try {
            const response = await fetch(`recipes/${list}`);
            const data = await response.json();
            const ArrayData = Array.from(data);
            setRecipes(ArrayData);
        } catch (e) {
            setRecipes(false);
            console.log(e);
        }
    }

    useEffect(() => {
        showRecipes();
    }, [list]);

    return (
        <WrapperRecipes>
            <Navigation history={history} />
            <div>
                <h1>My Recipes</h1>
                <CircleButton onClick={() => history.push('/addrecipe')}>Add New Recipe</CircleButton>
                <WrapperItemsDiv>
                    <RecipesItems recipes={recipes} showRecipes={showRecipes} />
                </WrapperItemsDiv>
            </div>
        </WrapperRecipes>
    );
}

export default Recipes;