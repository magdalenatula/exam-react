import React, { useState, useEffect, useContext } from "react";
import styled from "styled-components";
import io from "socket.io-client";
import { useForm } from "react-hook-form";
import { UserContext } from "../../common/UserContext";
import { ListContext } from "../../common/ListContext";
import Navigation from "../../components/Navigation"
// import queryString from "query-string";

const WrapperChat = styled("div")`
position:relative;
background: white;
display: flex;
h1 {
    color: #313842;
}
background: linear-gradient(
    to right bottom,
    rgba(255,255,255, 0.5),
    rgba(255,255,255, 0.3)
    );
backdrop-filter: blur(.2rem);
width: 85vw;
height: 85vh;
margin: auto;
border-radius: 20px;
z-index:21;
margin-top:4em;
`

const Chat = () => {
    const { user } = useContext(UserContext);
    const { list } = useContext(ListContext);
    const [household, setHousehold] = useState("1");
    const [name, setName] = useState("Magda");
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);
    const [users, setUsers] = useState('');
    const ENDPOINT = "localhost:8000"
    let socket;
    const { register, handleSubmit } = useForm();

    console.log(household)

    // const userProfile = async () => {
    //     try {
    //         const response = await fetch(`auth/profile/${user}`);
    //         const data = await response.json();
    //         setName(data[0].firstName)
    //     } catch (e) {
    //         setName(null);
    //         console.log(e);
    //     }
    // }


    useEffect(() => {
        socket = io(ENDPOINT);
        console.log(socket);
        // setHousehold(list);
        // userProfile();
        socket.emit('join', { name, household }, () => {
        });

        // return () => {
        //     socket.emit("disconnect");

        //     socket.off();
        // }
    }, [ENDPOINT, user, list]);

    useEffect(() => {
        socket.on('message', message => {
            setMessages(messages => [...messages, message]);
        });
        socket.on("householdData", ({ users }) => {
            setUsers(users);
        });
    }, []);

    const onSubmit = (data) => {
        setMessage(data.message);
        if (message) {
            console.log("working")
            // socket.emit('sendMessage', message, () => setMessage(''));
            // socket.emit('sendMessage', message, () => {
            //     setMessage("");
            // });
            socket.emit('sendMessage', { message }, () => {
            });
        }
    }

    return (
        <WrapperChat>
            <Navigation />
            <div>
                <h1>Chat page</h1>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <input name="message" type="text" ref={register({ required: true })} />
                    <button type="submit">Send</button>
                </form>
            </div>
        </WrapperChat>
    )
}

export default Chat;