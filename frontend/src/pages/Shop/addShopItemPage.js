import React, { useState, useContext } from "react";
import axios from 'axios';
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { ListContext } from "../../common/ListContext";
import Navigation from "../../components/Navigation";
import Confirmation from "../../components/Confirmation";
import ErrorMessage from "../../components/ErrorMessage";
import FoodSelection from "./components/FoodSelection"


const WrapperAddItem = styled("div")`
position:relative;
background: white;
display: flex;
h1 {
    color: #313842;
}
  }
background: linear-gradient(
    to right bottom,
    rgba(255,255,255, 0.5),
    rgba(255,255,255, 0.3)
    );
backdrop-filter: blur(.2rem);
width: 85vw;
height: 85vh;
margin: auto;
border-radius: 20px;
z-index:21;
margin-top:4em;
.empty-fridge-illustration {
    width: 20%;
  }
// form {
//     div{
//         margin-top:1em;
//         margin-bottom:1em;
//     }
// }
`
const FormWrapper = styled("form")`

    margin:auto;
    margin-top:3em;
`
const RightSide = styled("div")`
width:100%;
`
const QuantityInput = styled("input")`
    display: block;
    box-sizing: border-box;
    height: 40px;
    margin-right:1em;
    font-size: 0.9em;
    border: none;
    border-radius: 20px;
    padding: 2px 15px;
    background-color: rgba(255,255,255, 0.8); 
    &:focus {
        outline: none;
        box-shadow: 0 0 0 2px #8E47D1;
    }
    margin: auto;
    margin-bottom: 1em;
`
const CircleButton = styled("button")`
display: block;
margin: auto;
height: 40px;
padding: 0.5em 2em;
background: #5857D3;
color: white;
border-radius: 20px;
border: none;
font-weight: bold;
&:hover {
    opacity: 0.7;
}
&:focus {
    outline: none;
}
`
const AddShopItem = () => {
    const { list } = useContext(ListContext);
    const [confirmation, setConfirmation] = useState(false);
    const [error, setError] = useState(false);
    const [addItem, setaddItem] = useState(
        { foodID: 0, quantity: 0, listID: 0, toBuy: 1 }
    );

    const history = useHistory();

    const handleChange = (event) => {
        setaddItem({
            ...addItem,
            listID: list,
            toBuy: 1,
            [event.target.name]: parseFloat(event.target.value),
        })
    }

    const handleAddItem = async (event) => {
        event.preventDefault();
        console.log(addItem)
        const connection = await axios.post("shop/addItem/", addItem);
        console.log(connection);
        if (connection.data === "exists") {
            console.log("item already exists")
            setError(true);
        } else {
            setConfirmation(true);
        }
        setTimeout(() => setConfirmation(false), 2300);
    };

    return (
        <WrapperAddItem>
            <Navigation history={history} />
            <RightSide>
                <h1>Add an Item to your Shopping List</h1>
                {confirmation && <Confirmation text={"Item added to your Shopping List!"} />}
                {error && <ErrorMessage text={"Item already exists in your Shopping List!"} />}
                <FormWrapper id="itemForm" method="POST" onSubmit={handleAddItem}>
                    <FoodSelection handleChange={handleChange} />
                    <QuantityInput name="quantity" value={addItem.quantity} type="number" pattern="[0-9]*" onChange={handleChange} required />
                    <CircleButton type="submit">Add to my shopping list</CircleButton>
                </FormWrapper>
            </RightSide>
        </WrapperAddItem>
    );
}

export default AddShopItem;
