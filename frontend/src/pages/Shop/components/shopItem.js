import React, { useState } from "react";
import axios from 'axios';
import styled from "styled-components";
import EditShopItem from "./EditShopItem"
import { ReactComponent as DeleteIcon } from "../../../assets/ico_delete.svg";
import { ReactComponent as EditIcon } from "../../../assets/ico_edit.svg";

const OneItemBox = styled("div")`
    display: grid;
    border-style: solid;
    grid-template-columns:1fr 1fr 1fr 1fr 1fr;
    background-color: white;
    // padding: 3%;
    border-radius: 20px;
    justify-content: space-around;
    div{
        margin:auto;
        svg{
            width:30px;
        }
    }
    margin-bottom: 1em;
`;
const MoveButton = styled("button")`
padding: .3em 1.5em;
border-radius: 8px;
border-style: solid;
background-color: white;
margin-right: .5em;
margin-top: .5em;
`;

const ShopItem = (props) => {
    const { shopitem, showShoppingItems } = props;
    const [editItem, setEditItem] = useState(false);

    const handleDeleteItem = async () => {
        const connection = await axios.delete(`shop/${shopitem.itemID}`)
        showShoppingItems();
    };

    const handleAddToFridge = async () => {
        console.log(shopitem.itemID)
        const connection = await axios.patch(`shop/move/${shopitem.itemID}`, { 'foodID': shopitem.foodID, 'itemID': shopitem.itemID })
        console.log(connection);
        showShoppingItems();
    }

    const handleEditItem = async () => {
        setEditItem(true);
    }

    return (
        <div>
            {editItem && <EditShopItem showShoppingItems={showShoppingItems} setEditItem={setEditItem} itemID={shopitem.itemID} />}
            <OneItemBox className={`
                    ${shopitem.foodCategoryName === "Vegetables" ? "Vegetables" : ""} 
                    ${shopitem.foodCategoryName === "Fruit" ? "Fruit" : ""}
                    ${shopitem.foodCategoryName === "Meat" ? "Meat" : ""}
                    ${shopitem.foodCategoryName === "Dairy" ? "Dairy" : ""}
                    ${shopitem.foodCategoryName === "Fish" ? "Fish" : ""}
                    `}>
                <div>
                    <h3>{shopitem.foodName}</h3>
                </div>
                <div>
                    <h3>{shopitem.quantity}</h3>
                </div>
                <div>
                    <MoveButton onClick={handleAddToFridge}>Move to Fridge</MoveButton>
                </div>
                <div>
                    <DeleteIcon onClick={handleDeleteItem} className="ico_delete" />
                </div>
                <div>
                    <EditIcon onClick={handleEditItem} className="ico_edit" />
                </div>
            </OneItemBox>
        </div>
    );
}

export default ShopItem;
