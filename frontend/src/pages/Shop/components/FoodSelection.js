import React, { useEffect, useState } from "react";
import axios from 'axios';
import styled from "styled-components";

const WrapperItemsDiv = styled("div")`
    position: relative;
    height: 50vh;
    overflow: auto;
    border-radius: 20px;
    padding: 1em;
    margin: auto;
    margin-top: 1em;
    display: grid; 
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    grid-column-gap: 1em;
    grid-row-gap: 1em;
    `

const FoodItemDiv = styled("div")`
    background-color:white;
    padding: 3%;
    align-content: center;
    justify-content: space-around;
    border-radius: 5px;
    border-style: solid;
    &:hover {
        opacity: 0.7;
      }
`

const SearchInput = styled("input")`
    box-sizing: border-box;
    width: 400px;
    height: 40px;
    margin-right:1em;
    font-size: 0.9em;
    border: none;
    border-radius: 20px;
    padding: 2px 15px;
    background-color: rgba(255,255,255, 0.4); 
    &:focus {
        outline: none;
        box-shadow: 0 0 0 2px #8E47D1;
    }
`
const FoodInput = styled("input")`
    // visibility:hidden;
    &:checked  {
        background: red;
    }
`
const FoodSelection = ({ handleChange }) => {
    const [search, setSearch] = useState("");
    const [searchedItems, setSearchedItems] = useState([]);
    const [food, setFood] = useState([]);

    const getFood = async () => {
        try {
            const result = await axios.get("food/");
            if (result && result.data.data !== undefined) {
                setFood(result.data.data);
            } else {

            }
        } catch (e) {
            console.log(e)
            setFood(false);
        }
    }
    useEffect(() => {
        getFood();
    }, []);

    useEffect(() => {
        setSearchedItems(
            food.filter((fooditem) =>
                fooditem.foodName.toLowerCase().includes(search.toLowerCase())
            )
        );
    }, [search, food]);


    return (
        <div>
            <SearchInput
                type="text"
                placeholder="Search Food Items"
                onChange={(e) => setSearch(e.target.value)}
            />
            <WrapperItemsDiv className="food-items">
                {searchedItems && searchedItems.map((fooditem) => (
                    <FoodItemDiv className={`
                    ${fooditem.foodCategoryName === "Vegetables" ? "Vegetables" : ""} 
                    ${fooditem.foodCategoryName === "Fruit" ? "Fruit" : ""}
                    ${fooditem.foodCategoryName === "Meat" ? "Meat" : ""}
                    ${fooditem.foodCategoryName === "Dairy" ? "Dairy" : ""}
                    ${fooditem.foodCategoryName === "Fish" ? "Fish" : ""}
                    `}>
                        <FoodInput type="radio" name="foodID" value={fooditem.foodID} onChange={handleChange} />
                        < h3 > {fooditem.foodName}</h3 >
                    </FoodItemDiv >
                ))}

            </WrapperItemsDiv>
        </div>
    )
}

export default FoodSelection