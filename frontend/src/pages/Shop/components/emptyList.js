import React from "react";
import styled from "styled-components";
import empty_list from "../../../assets/empty_list.png";

const WrapperDiv = styled("div")`
    img {    
        margin-top:2em;
        width:15%;
    }
`
const EmptyList = () => {
    return (
        <WrapperDiv>
            <img src={empty_list} alt="Logo" />
            <h1>It’s Empty!</h1>
            <p>Looks like you haven’t saved
                any items yet… </p>
        </WrapperDiv>
    );
}

export default EmptyList;