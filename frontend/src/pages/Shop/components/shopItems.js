import React from "react";
import styled from "styled-components";
import ShopItem from './ShopItem';

const ShopItemsWrapper = styled("div")`
justify-content: space-around;
`;

const ShopItems = (props) => {
    const { searchedItems, showShoppingItems } = props;
    return (
        <ShopItemsWrapper>
            {searchedItems && searchedItems.map((shopitem, index) => (
                <ShopItem showShoppingItems={showShoppingItems} key={index} shopitem={shopitem} />
            ))}
        </ShopItemsWrapper>
    );
}
export default ShopItems;
