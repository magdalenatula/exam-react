import '../App.css';
import React, { useEffect, useState, useMemo } from "react";
import { BrowserRouter as Router } from 'react-router-dom';
import styled from "styled-components";
import { UserContext } from '../common/UserContext';
import { ListContext } from '../common/ListContext';
import Routes from "../Routes/Routes";

const AppWrapper = styled("div")`
  overflow: hidden;
  position: relative;
  justify-content: center;
  align-items: center;
  text-align: center;
  background: linear-gradient(45deg, #D1D2D3 0%, #F4F4F4 100%);
  height: 100vh;
  background-repeat: no-repeat;
  background-size: cover;
`
const Circle1 = styled("div")`
  height: 20rem;
  width: 20rem;
  position: absolute;
  background: white;
  background: linear-gradient(45deg, #5857D3 0%, #DDDAE9 100%);
  bottom: 2%;
  left:4%;
  z-index:1;
  border-radius: 50%;
`
const Circle2 = styled("div")`
  height: 20rem;
  width: 20rem;
  position: absolute;
  background: white;
  background: linear-gradient(45deg, #8883DB 0%, #DDDAE9 100%);
  top: 2%;
  right:3%;
  z-index:1;
  border-radius: 50%;
`
const Circle3 = styled("div")`
  height: 10rem;
  width: 10rem;
  position: absolute;
  background: white;
  background: linear-gradient(45deg, #8883DB 0%, #DDDAE9 100%);
  bottom: 2%;
  left:4%;
  z-index:3;
  border-radius: 50%;
`

const App = () => {
  const [user, setUser] = useState();
  const [list, setList] = useState();
  const providerUser = useMemo(() => ({ user, setUser }), [user, setUser])
  const providerList = useMemo(() => ({ list, setList }), [list, setList])

  const getSession = async () => {
    try {
      const response = await fetch("/session/getcurrent");
      const data = await response.json();
      if (data && data.data !== undefined) {
        await setUser(data.data[0].userID);
        await setList(data.data[0].listID);
      }
    } catch (e) {
      setUser(false);
      setList(false);
      console.log(e);
    }
  }

  useEffect(() => {
    getSession();
  }, [user, list]);

  return (
    <div>
      <AppWrapper>
        <UserContext.Provider value={providerUser}>
          <ListContext.Provider value={providerList}>
            <Router>
              <div><Routes /></div>
            </Router>
          </ListContext.Provider>
        </UserContext.Provider>
      </AppWrapper>
      <Circle1></Circle1>
      <Circle2></Circle2>
      <Circle3></Circle3>
    </div>
  );
}

export default App;

