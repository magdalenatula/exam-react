import React, { useRef, useEffect, useState } from "react";
import { NavLink } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import styled from "styled-components";
// import gsap, { TweenMax } from "gsap";
import { ReactComponent as FridgeIcon } from "../assets/ico_fridge.svg";
import { ReactComponent as LogoutIcon } from "../assets/ico_logout.svg";
import { ReactComponent as ProfileIcon } from "../assets/ico_profile.svg";
import { ReactComponent as ShopIcon } from "../assets/ico_clipboard.svg";
import { ReactComponent as RecipesIcon } from "../assets/ico_recipes.svg";
import { ReactComponent as ChatIcon } from "../assets/ico_chat.svg";
import { ReactComponent as FridgeIllustration } from "../assets/fridge_illustration.svg";

const NavBar = styled("nav")`
  height:81.5vh;
  margin: auto;
  position: relative;
  display: block;
  background: linear-gradient(
    to right bottom,
    rgba(255,255,255, 0.7),
    rgba(255,255,255, 0.5)
    );
  border-radius: 20px 0px 0px 20px;
  justify-content: space-evenly;
  padding:1em;
  .nav-ico {
    width: 30px;
    margin: auto 0;
    margin-right:1em;
  }
  width: 15vw;
  z-index:21;
  .fridge_ill{
    width: 60%;
    .st0{
        fill: white;
    }
    .active {
        background-color:red;
    }

`

const NavLinkStyled = styled(NavLink)`
cursor: pointer;
text-decoration: none;
color: #4A4A4A;
display:flex;
margin-top:2em;
svg{
    text-align: left; 
    display:flex;
}
&:hover {
    opacity:65%;
    }
    p{
        font-size:.8em;
        text-align: left;
    }

`

const TabIndicator = styled("span")`
position: absolute;
top:0;
height: 50px;
width: 4px;
background-color: #5857D3;
z-index: 9999;
display: block;
border-radius: 30px;
`

const Navigation = () => {
    const history = useHistory();
    const elements_wrapper = useRef(null);
    const elements_indicator = useRef(null);
    const [active, setActive] = useState();

    // useEffect(() => {
    //     const elements = elements_wrapper.current.children;
    //     const indicator = elements_indicator.current;

    //     const getActiveNavElement = () => {
    //         return Array.from(elements).findIndex(element => element.classList.contains("active"));
    //     }

    //     const getIndicatorPosition = () => {
    //         const { height: elementHeight, y: elementY } = elements[getActiveNavElement()].getBoundingClientRect();
    //         const { height: indicatorHeight, y: indicatorY } = indicator.getBoundingClientRect();
    //         return elementY + elementHeight / 2 - indicatorHeight / 2;
    //     }

    //     const setIndictorPosition = () => {
    //         // const tl = gsap.timeline({ defaults: { ease: 'power3.inOut' } });
    //         // tl.fromTo(indicator, { x: getIndicatorPosition() }, { duration: 1, x: getIndicatorPosition() })
    //         TweenMax.set(indicator, { y: getIndicatorPosition() });
    //     }

    //     const changendicatorPosition = (e) => {
    //         TweenMax.to(indicator, .5, { y: getIndicatorPosition() });
    //     }
    //     setIndictorPosition();

    //     window.addEventListener('resize', setIndictorPosition);

    // }, [])

    return (
        <div>
            {/* <TabIndicator ref={elements_indicator}></TabIndicator> */}
            <NavBar>
                <FridgeIllustration className="fridge_ill" />
                <div ref={elements_wrapper}>
                    <NavLinkStyled onClick={() => history.push("/fridge")} activeClassName="active" to="/fridge">
                        <FridgeIcon className="nav-ico" /><p>Fridge</p></NavLinkStyled>

                    <NavLinkStyled onClick={() => history.push("/shop")} activeClassName="active" to="/shop">
                        <ShopIcon className="nav-ico" /><p>Shopping List</p></NavLinkStyled>

                    <NavLinkStyled onClick={() => history.push("/recipes")} activeClassName="active" to="/recipes">
                        <RecipesIcon className="nav-ico" /><p>My Recipes</p></NavLinkStyled>

                    <NavLinkStyled onClick={() => history.push("/random")} activeClassName="active" to="/random">
                        <RecipesIcon className="nav-ico" /><p>Inspirations</p></NavLinkStyled>

                    <NavLinkStyled className="disable" onClick={() => history.push("/chat")} activeClassName="active" to="/chat">
                        <ChatIcon className="nav-ico" /><p>Chat</p>
                    </NavLinkStyled>

                    <NavLinkStyled onClick={() => history.push("/profile")} activeClassName="active" to="/profile" >
                        <ProfileIcon className="nav-ico" /><p>Profile</p></NavLinkStyled>

                    <NavLinkStyled onClick={() => history.push("/logout")} activeClassName="active" to="/logout">
                        <LogoutIcon className="nav-ico" /><p>Logout</p></NavLinkStyled>
                </div>
            </NavBar>
        </div>
    );
};

export default Navigation;
