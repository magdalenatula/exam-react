import React from "react";
import styled, { keyframes } from "styled-components";
import Check from "../assets/check.png";


const popupAnim = (top) => keyframes`
  20% {
    opacity: 1;
  }
  30% {
    top: ${top};
  }
  70% {
    opacity: 1;
  }
  85% {
    top: ${top};
  }
`;

const Container = styled("div")`
  font-size: 1.1em;
  font-weight: bold;
  position: fixed;
  opacity: 0;
  top: 0;
  right: 42px;
  height: 2em;
  width: auto;
  background-color: white; 
  border-radius: 20px;
  padding: 0.6em 1.1em 0;
  overflow: hidden;
  animation: ${popupAnim("1em")} 3s ease;
  z-index: 5000;
  border-style: solid;
  border-color: #32BEA6;
  img{
    width: 20px;
    margin-right: 1em;
  }
`;

const Confirmation = ({ text }) => (
  <Container>
    <img src={Check} alt="Logo" />
    {text}
  </Container>
);

export default Confirmation;