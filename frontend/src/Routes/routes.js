import React, { useContext } from "react";
import { Switch, Route, Redirect } from 'react-router-dom';
import { ListContext } from '../common/ListContext';
import Fridge from "../pages/Fridge/FridgePage";
import ShoppingList from "../pages/Shop/ShopListPage";
import Login from "../pages/Auth/LoginPage";
import Signup from "../pages/Auth/SignupPage";
import Household from "../pages/Auth/HouseholdPage";
import Recipes from "../pages/Recipes/RecipesPage";
import Logout from "../pages/Auth/LogoutPage";
import AddFridgeItem from "../pages/Fridge/AddFridgeItemPage";
import AddShopItem from "../pages/Shop/AddShopItemPage";
import AddRecipeItem from "../pages/Recipes/AddRecipeItemPage";
import RandomRecipePage from "../pages/RandomRecipes/RandomRecipesPage";
import Users from "../pages/Profile/ProfilePage";
import Chat from "../pages/Chat/ChatPage";

const Routes = () => {
    const { list } = useContext(ListContext);
    return (
        <div>
            <Switch>
                <Route exact path="/">
                    {list ? <Redirect to="/fridge" /> : <Redirect to="/login" />}
                </Route>
                <Route path="/addshopitem" component={AddShopItem} />
                <Route path="/addfridgeitem" component={AddFridgeItem} />
                <Route path="/addrecipe" component={AddRecipeItem} />
                <Route path="/profile" component={Users} />
                <Route path="/login" component={Login} />
                <Route path="/signup" component={Signup} />
                <Route path="/household" component={Household} />
                <Route path="/logout" component={Logout} />
                <Route path='/fridge' component={Fridge} />
                <Route path='/shop' component={ShoppingList} />
                <Route path='/recipes' component={Recipes} />
                <Route path='/random' component={RandomRecipePage} />
                <Route path='/chat' component={Chat} />
            </Switch>
        </div>
    );
};

export default Routes;
