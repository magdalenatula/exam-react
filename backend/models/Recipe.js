const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const RecipeSchema = new mongoose.Schema(
    {
        recipeName: {
            type: String,
            required: [true, "can't be blank"],
        },
        recipeDescription: String,
        recipeIngridients: [String],
        recipeListID: Number,
    },
    { timestamps: true }
);

RecipeSchema.plugin(uniqueValidator, { message: "already exists" });

const RecipeModel = mongoose.model("Recipe", RecipeSchema);
module.exports = RecipeModel;
