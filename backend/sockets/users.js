const users = [];

const addUser = ({ id, name, household }) => {
    // name = name.trim().toLowerCase();
    // household = household.trim().toLowerCase();
    // const existingUser = users.find((user) => user.household === household && user.name === name);
    const user = { id, name, household };
    users.push(user)
    // console.log(users);
    return { user }
}

const removeUser = (id) => {
    const index = users.findIndex((user) => user.id === id);

    if (index !== -1) return users.splice(index, 1)[0];
}

const getUser = (id) => users.find((user) => user.id === id);

const getUsersInHousehold = (household) => users.filter((user) => user.household === household)

module.exports = { addUser, removeUser, getUser, getUsersInHousehold }