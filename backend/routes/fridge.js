'use strict';

const router = require('express').Router();
const mysql = require('mysql');


///    -----   DATABASE CONNECTION   -----    ///


const db = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT
})


///    -----   SELECT ALL ITEMS IN THE FRIDGE FOR THE SPECIFIC LIST ID   -----    ///

router.get('/:listID', (req, res) => {
    const { listID } = req.params;
    const sql = "SELECT Item.itemID, Item.quantity, Food.foodName, FoodCategories.foodCategoryName FROM Item JOIN Food ON Item.foodID = Food.foodID JOIN FoodCategories ON Food.foodCategoryID = FoodCategories.foodCategoryID WHERE `listID`='" + listID + "'and toBuy = false;";
    db.query(sql, (err, data) => {
        if (err) {
            console.log(err);
        }
        res.send(data);
    })
});


///    -----   CREATE A FRIDGE ITEM   -----    ///

router.post('/addItem', (req, res) => {
    const { foodID, listID, toBuy, quantity } = req.body;
    const checkExisting = "SELECT * FROM Item WHERE listID = '" + listID + "' and toBuy = '" + toBuy + "' and foodID = '" + foodID + "';"
    db.query(checkExisting, (err, rows) => {
        if (err) {
            return res.status(500).send({ err });
        }
        if (!rows.length) {
            const sqlAddItem = `INSERT INTO Item (foodID, toBuy, quantity, listID) VALUES (?);`;
            const foodValues = [
                foodID,
                toBuy,
                quantity,
                listID
            ];
            db.query(sqlAddItem, [foodValues], (err, data) => {
                if (err) {
                    return res.status(500).send({ err });
                }
                res.send({ data });
            })
        } else {
            return res.send("exists");
        }
    })
});



///    -----   UPDATE AN ITEM FROM THE FRIDGE   -----    ///

router.patch('/:itemID', (req, res) => {
    const { quantity, itemID } = req.body;
    const sql = "UPDATE Item SET  quantity ='" + quantity + "' WHERE `itemID`='" + itemID + "' ;";
    const foodValues = [
        quantity,
    ];
    db.query(sql, [foodValues], (err, data) => {
        if (err) {
            return res.send({ err });
        }
        res.send({ data })
    });
});


///    -----   DELETE AN ITEM FROM THE LIST   -----    ///

router.delete('/:itemID', (req, res) => {
    const { itemID } = req.params;
    const sql = "DELETE FROM Item WHERE `itemID`='" + itemID + "' ;";
    db.query(sql, (err, data) => {
        if (err) {
            return res.send({ err });
        }
        res.send({ data })
    });
});

module.exports = router;