'use strict';

const router = require('express').Router();
const mysql = require('mysql');


///    -----   DATABASE CONNECTION   -----    ///

const db = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT
})


router.get('/', (req, res) => {
    const sql =
        `SELECT Food.foodID, Food.foodName, FoodCategories.foodCategoryName
    FROM Food
    JOIN FoodCategories
    ON Food.foodCategoryID = FoodCategories.foodCategoryID;`;
    db.query(sql, (err, data) => {
        if (err) {
            console.log(err);
        }
        res.json({ data })
    })
});


module.exports = router;