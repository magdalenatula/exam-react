const router = require('express').Router();
const RecipeModel = require("../models/Recipe");

router.get("/:recipeListID", async (req, res) => {
    const { recipeListID } = req.params;
    console.log(recipeListID)
    try {
        const recipes = await RecipeModel.find({ recipeListID });
        return res.send(recipes);
    } catch (error) {
        return res.send({ error });
    }
});

router.post("/addItem", async (req, res) => {
    const recipe = new RecipeModel(req.body);
    recipe.save()
        .then((result) => {
            res.json({
                message: "successfully created",
            });
        })
        .catch((err) => {
            res.json({
                message: 'unable to create',
            });
        });
});

router.delete("/:recipeID", async (req, res) => {
    const { recipeID } = req.params;
    try {
        await RecipeModel.deleteOne({ _id: recipeID });
        const updatedRecipes = await RecipeModel.find({});
        return res.send({ data: updatedRecipes });
    } catch (error) {
        return res.send({ error });
    }
});

router.delete("/:recipeID", async (req, res) => {
    const { recipeID } = req.params;
    try {
        await RecipeModel.deleteOne({ _id: recipeID });
        const updatedRecipes = await RecipeModel.find({});
        return res.send({ data: updatedRecipes });
    } catch (error) {
        return res.send({ error });
    }
});

// router.patch('/:recipeID', async (req, res) => {
//     const {recipeIngridients, recipeName, recipeDescription, recipeListID} =req.body;

// });


module.exports = router;