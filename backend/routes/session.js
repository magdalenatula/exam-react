const router = require("express").Router();
const mysql = require('mysql');


const db = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT
})


///    -----   GET A CURRENT LOGGED IN USER   -----    ///

router.get("/getcurrent", (req, res) => {
    const { userID } = req.session;
    const sql = "SELECT * FROM `User` WHERE `userID`='" + userID + "'";
    if (userID) {
        db.query(sql, (err, data) => {
            return res.send({ data });;
        });
    } else {
        return res.send({ user: null });
    }
});

module.exports = router;
