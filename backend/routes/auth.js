'use strict';

const router = require('express').Router();
const mysql = require('mysql');
const bcrypt = require("bcrypt");


///    -----   DATABASE CONNECTION   -----    ///

const db = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT
})


///    -----   SIGNUP   -----    ///

router.post('/signup', async (req, res) => {
    const { lastName, firstName, email, password, confirmPassword } = req.body;
    const hash = await bcrypt.hash(password, 10);
    const listSql = `INSERT INTO List (listID) VALUES (null);`;

    if (password !== confirmPassword) {
        return res
            .status(500)
            .send({ error: "Password is not identical.Try again." });
    }
    const checkExistingUser = "SELECT * FROM User WHERE email='" + email + "';";
    db.query(checkExistingUser, (err, rows) => {
        if (err) {
            return res.status(500).send({ err });
        }
        if (!rows.length) {
            db.query(listSql, (err, result) => {
                if (err) {
                    return res.status(500).send({ err });
                }
            });
            let sql = `SELECT listID FROM List ORDER BY listID DESC LIMIT 1;;`;
            db.query(sql, (err, data) => {
                if (err) throw err;
                if (data) {
                    const lastID = data[0].listID;
                    const userSql = `INSERT INTO User (lastName, firstName, email, password, listID, listAccess) VALUES (?)`;
                    const userValues = [
                        lastName,
                        firstName,
                        email,
                        hash,
                        lastID,
                        true
                    ];
                    db.query(userSql, [userValues], (err, data) => {
                        if (err) {
                            return res.status(500).send({ err });
                        }
                        res.send({ data });
                    });
                }
            });
        } else {
            return res.send("exists");
        }
    });
});


///    -----   SIGNUP TO HOUSEHOLD WITH LIST ID   -----    ///

router.post('/household', async (req, res, next) => {
    const { lastName, firstName, email, password, confirmPassword, listID } = req.body;
    const hash = await bcrypt.hash(password, 10);

    if (password !== confirmPassword) {
        return res
            .status(500)
            .send({ error: "Password is not identical.Try again." });
    }
    const checkExistingUser = "SELECT * FROM User WHERE email='" + email + "';";
    db.query(checkExistingUser, (err, rows) => {
        if (err) {
            return res.status(500).send({ err });
        }
        if (!rows.length) {
            const userSql = `INSERT INTO User (lastName, firstName, email, password, listID, listAccess) VALUES (?)`;
            const userValues = [
                lastName,
                firstName,
                email,
                hash,
                listID,
                true
            ];
            db.query(userSql, [userValues], (err, data) => {
                if (err) {
                    return res.status(500).send({ err });
                }
                res.send({ data });
            });

        } else {
            return res.send("exists");
        }
    });
});


///    -----   LOGIN   -----    ///

router.post('/login', async (req, res) => {
    const { email, password } = req.body;
    const loginSql = "SELECT * FROM `User` WHERE `email`='" + email + "'";
    if (email && password) {
        await db.query(loginSql, (err, result) => {
            if (result.length > 0) {
                bcrypt.compare(password, result[0].password, (err, response) => {
                    if (response) {
                        console.log(req.session)
                        req.session.userID = result[0].userID;
                        console.log(req.session.userID);
                        const userID = result[0].userID;
                        res.status(200).send({ userID });
                    } else {
                        res.status(401).send({ message: "email or password is incorect" })
                    }
                })
            }
        });
    } else {
        res.send('Please enter Username and Password!');
    }
});


///    -----      LOGOUT      -----    ///

router.get("/logout", (req, res) => {
    req.session.destroy((err) => {
        if (err) {
            return res.status(500).send({ error: err });
        }
        return res.status(200).send({ data: "Logged out" });
    });
});


///    -----  HOUSEHOLD MEMBERS INFORMATION    -----    ///

router.get('/:listID', (req, res) => {
    const { listID } = req.params;
    const sql = "SELECT * FROM User WHERE listID ='" + listID + "';";
    db.query(sql, (err, result) => {
        if (err) {
            console.log(err);
        }
        res.json(result)
    });
});

router.get('/profile/:userID', (req, res) => {
    const { userID } = req.params;
    const sql = "SELECT * FROM User WHERE userID ='" + userID + "';";
    db.query(sql, (err, result) => {
        if (err) {
            console.log(err);
        }
        res.json(result)
    });
});

module.exports = router;