const express = require("express");
const session = require("express-session");
const rateLimiter = require("express-rate-limit");
const mongoose = require("mongoose");
const socketio = require('socket.io');
const http = require('http');
// const cors = require("cors");
const { addUser, removeUser, getUser, getUsersInHousehold } = require("./sockets/users")

require('dotenv').config();

const app = express();
const socket_app = express();
const server = http.createServer(socket_app);
const io = socketio(server);

///    -----   SOCKETS   -----    ///

io.on('connection', (socket) => {
    console.log("we have a new connection!");

    socket.on('join', ({ name, household }, callback) => {
        // console.log(name, household, socket.id);

        const { user } = addUser({ id: socket.id, name, household });

        socket.join(user.household);

        socket.emit('message', { user: "admin", text: `${user.name}, Welcome to the household ${user.household} Chat` });
        socket.broadcast.to(user.household).emit('message', { user: 'admin', text: `${user.name} has joined!` });

        io.to(user.household).emit('householdData', { room: user.household, users: getUsersInHousehold(user.household) });

        callback();
    })

    socket.on('sendMessage', (message, callback) => {
        const user = getUser(socket.id);
        console.log(message, user)
        io.to(user.household).emit('message', { user: user.name, text: message });

        callback();
    })

    socket.on('disconnect', () => {
        console.log("user has left");
        const user = removeUser(socket.id);

        if (user) {
            io.to(user.household).emit('message', { user: 'Admin', text: `${user.name} has left.` });
            io.to(user.household).emit('householdData', { household: user.household, users: getUsersInHousehold(user.household) });
        }
    })
});


///    -----   MIDDLEWARE   -----    ///

// app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
socket_app.use(express.json());
socket_app.use(express.urlencoded({ extended: true }));


///    -----   ROUTES   -----    ///

const usersRouter = require('./routes/auth');
const sessionRouter = require('./routes/session');
const foodRouter = require('./routes/food');
const fridgeRouter = require('./routes/fridge');
const shopRouter = require('./routes/shop');
const recipesRouter = require('./routes/recipes');
const chatRouter = require('./routes/chat');

///    -----   SESSION   -----    ///

app.use(
    session({
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: false,
        cookie: { secure: false },
    })
);

app.use(
    rateLimiter({
        windowMs: 10 * 60 * 1000, // 10 minutes
        max: 200, // limit each IP to 200 requests per windowMs

    })
);

app.use(
    "/auth/",
    rateLimiter({
        windowMs: 10 * 60 * 1000,
        max: 20,
    })
);


app.use('/auth', usersRouter);
app.use('/session', sessionRouter);
app.use('/food', foodRouter);
app.use('/fridge', fridgeRouter);
app.use('/shop', shopRouter);
app.use('/recipes', recipesRouter);
socket_app.use('/chat', chatRouter);


// ------------ DB CONNECTION ------------ //

// mongodb atlas connection
mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
});


///    -----   SERVER CONNECTION   -----    ///

const PORT = process.env.PORT || 5000;
const SOCKET_PORT = process.env.SOCKET_PORT || 8000;

server.listen(SOCKET_PORT, () => console.log(`Socket Server is running on port ${SOCKET_PORT}`));


app.listen(PORT, (error) => {
    if (error) {
        console.log("Error running the server");
    }
    console.log("Server is running on port", Number(PORT));
});
