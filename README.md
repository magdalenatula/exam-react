# React/Node.js Exam Project

## WHAT'S IN MY FRIDGE? APP 

The idea of the project is shared fridge app.
- User can signup/login to the system. 
- After login to the system user has an overview of the items in his/her fridge.
- User can add new items to the fridge (App has built-in food database).
- User can edit/delete items in his/her fridge.
- User can create shopping list
- User can edit/delete items from the shopping list
- User can move items from shopping list directly to the fridge list
- User has an overview of the profile information and household members
- User can signup as a member of a household
- User can store Recipes which are assigned to Household
- User can create a new recipe 


## DATABASE

In my project I use two database types: mysql and mongoDB. 

## ENTITY RELATIONSHIP DIAGRAM

![](frontend/src/assets/erd.png)


## API ENDPOINTS

## Food

| Endpoint        | Method | Description                  | Request body/params                 |
| --------------- | ------ | ---------------------------- | ----------------------------------- |
| /food/          | GET    | show all                     |                                     |
| /food/add       | POST   | add new item                 | { foodName, foodCategoryID }        |


## Authentication

| Endpoint            | Method | Description                 | Request body/params                                               |
| ------------------- | ------ | --------------------------- | ----------------------------------------------------------------- |
| /session/getcurrent | GET    | get current user            |                                                                   |
| /auth/login         | POST   | login user                  | { email, password }                                               |
| /auth/signup        | POST   | add new user                | { lastName, firstName, email, password, confirmPassword }         |
| /auth/household     | POST   | add new user to a household | { lastName, firstName, email, password, confirmPassword, listID } |
| /auth/logout        | GET    | logout user                 |                                                                   |

## Fridge

| Endpoint        | Method | Description                  | Request body/params                 |
| --------------- | ------ | ---------------------------- | ----------------------------------- |
| /fridge/:listID | GET    | show all items in the fridge | { listID }                          |
| /fridge/addItem | POST   | show all items in the fridge | { foodID, listID, toBuy, quantity } |
| /fridge/:itemID | PATCH  | update quantuty in the item  | { quantity, itemID }                |
| /fridge/:itemID | DELETE | delete item from the list    | { itemID }                          |


## Shopping List

| Endpoint             | Method | Description                 | Request body/params                 |
| -------------------- | ------ | --------------------------- | ----------------------------------- |
| /shop/:listID        | GET    | show all items in the list  | { listID }                          |
| /shop/addItem        | POST   | show all items in the list  | { foodID, listID, toBuy, quantity } |
| /shop/:itemID        | PATCH  | update quantuty in the item | { quantity, itemID }                |
| /shop/:itemID        | DELETE | delete item from the list   | { itemID }                          |
| /shop/move/:itemID   | PATCH  | move item to the fridge     | { foodID, itemID }                  |


## Recipes

| Endpoint                | Method | Description                   | Request body/params                                                |
| ----------------------- | ------ | ----------------------------- | ------------------------------------------------------------------ |
| /recipes//:recipeListID | GET    | show all recipes for gived ID | { recipeListID }                                                   |
| /recipes/addItem        | POST   | add new recipe                | { recipeName, recipeIngridients, recipeListID, recipeDescription } |

